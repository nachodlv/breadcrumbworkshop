﻿#pragma once

// UE Includes
#include "CoreMinimal.h"

#include "BTWBreadcrumbActorListener.generated.h"


class IBTWCanJump;

/**
 * Listens to an actor jumping, landing and walking off ledge.
 * Once landed it will try to create a Breadcrumb using the BreadcrumbsGenerator
 */
UCLASS()
class ABTWBreadcrumbActorListener : public AActor
{
	GENERATED_BODY()

public:
	/** Sets the BreadcrumbsGenerator */
	void Initialize(class ABTWBreadcrumbsGenerator* NewBreadcrumbsGenerator);

	/** Binds to the actor jump, land and walk off ledge */
	void ListenToActor(const TScriptInterface<IBTWCanJump>& Actor);

	/** Unbinds from the ActorListened jump, land and walk off ledge */
	virtual void BeginDestroy() override;

private:
	/** Method called when the actor jumps. Sets the vector From */
	UFUNCTION()
	void ActorJumped(const FVector& Location);

	/** Method called when the actor walks off ledge. Sets the vector From */
	UFUNCTION()
    void ActorWalkedOffLedge(const FVector& PreviousPosition);

	/**
	 * Method called when the actor lands.
	 * Places a breadcrumb with the vector From and the position where it landed
	 */
	UFUNCTION()
	void ActorLanded(const FVector& Location);

	/** Used to set the breadcrumbs when the ActorListened lands */
	UPROPERTY(Transient)
	ABTWBreadcrumbsGenerator* BreadcrumbsGenerator = nullptr;

	/** Actor being listened */
	UPROPERTY(Transient)
	TScriptInterface<IBTWCanJump> ActorListened = nullptr;

	/** Position from where the actor jumped or walked off ledge */
	FVector From;

	/** Delegates */
	FDelegateHandle JumpHandle;
	FDelegateHandle WalkOffLedgeHandle;
	FDelegateHandle LandHandle;
};

