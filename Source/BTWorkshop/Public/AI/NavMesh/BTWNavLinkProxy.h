﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "Navigation/NavLinkProxy.h"

// BTW Includes
#include "Interfaces/BTWNavLinkProxyProvider.h"

#include "BTWNavLinkProxy.generated.h"

/** Link that launches the character from the starting position to the end position */
UCLASS()
class ABTWNavLinkProxy : public ANavLinkProxy, public IBTWNavLinkProxyProvider
{
	GENERATED_BODY()

public:
	ABTWNavLinkProxy();

protected:
	/** Binds to the OnSmartLinkReached from the parent class */
	virtual void BeginPlay() override;

	/** Launches the agent to the DestPoint. The agent should extend from ACharacter */
	UFUNCTION()
	void NotifySmartLinkReached(AActor* PathingAgent, const FVector& DestPoint);

	// ~ Begin IBTWNavLinkProxyProvider
	/** Returns a delegate that will be called when this nav link proxy is reached */
	virtual FNavLinkProxyReached& GetOnNavLinkProxyReached() override { return OnNavLinkProxyReached; }
	/** Returns the Smart Link Component from the parent class */
	virtual UNavLinkCustomComponent* GetSmartLink() override { return GetSmartLinkComp(); }
	// ~ End IBTWNavLinkProxyProvider

private:
	/** Calculates the velocity that needs to have the actor to reach its destination */
	FVector CalculateLaunchVelocity(const AActor* LaunchedActor, const FVector& Destination) const;

	FNavLinkProxyReached OnNavLinkProxyReached;
};
