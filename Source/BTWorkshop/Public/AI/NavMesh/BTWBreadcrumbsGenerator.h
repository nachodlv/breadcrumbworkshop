﻿#pragma once

// UE Includes
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

// BTW Includes
#include "Interfaces/BTWCanJump.h"

#include "BTWBreadcrumbsGenerator.generated.h"

class UNavLinkComponent;

USTRUCT(BlueprintType)
struct FBTWActorRegisteredHandle
{
	GENERATED_BODY()

	FBTWActorRegisteredHandle() {}
	FBTWActorRegisteredHandle(int32 InHandle) : Handle(InHandle) {}
	FBTWActorRegisteredHandle(const FBTWActorRegisteredHandle& InHandle) : Handle(InHandle.Handle) {}

	bool IsValid() const { return Handle != INDEX_NONE; }

	UPROPERTY(Transient)
	int32 Handle = INDEX_NONE;

	friend bool operator==(FBTWActorRegisteredHandle Lhs, FBTWActorRegisteredHandle Rhs)
	{
		return Lhs.Handle == Rhs.Handle;
	}

	friend bool operator!=(FBTWActorRegisteredHandle Lhs, FBTWActorRegisteredHandle Rhs)
	{
		return !operator==(Lhs, Rhs);
	}

	operator bool() const
	{
		return IsValid();
	}

	static FBTWActorRegisteredHandle GenerateNewHandle()
	{
		static int32 AttachmentHandleNumber = 0;
		++AttachmentHandleNumber;
		return FBTWActorRegisteredHandle(AttachmentHandleNumber);
	}
};

struct FBreadcrumbToBePlaced
{
	const FVector From;
	const FVector To;

	FBreadcrumbToBePlaced(const FVector InFrom, const FVector InTo): From(InFrom), To(InTo) {}
};

class ANavLinkProxy;
class UBTWNavLinkPooler;
class IBTWNavLinkProxyProvider;

/** Listens to the jump of registered actors to dynamically create custom links simulating this jump. */
UCLASS()
class ABTWBreadcrumbsGenerator : public AActor
{
	GENERATED_BODY()

public:
	ABTWBreadcrumbsGenerator();

	/**
	 * Binds a delegate to the path async result.
	 * Initializes the pooler ActorListenersPooler.
	 */
	virtual void BeginPlay() override;

	/** De activates all the ActorListeners */
	virtual void BeginDestroy() override;

	/**
	 * Registers an Actor that implements the interface IBTWCanJump.
	 * The BreadcrumbsGenerator will now listen to the jumps and falls from the actor.
	 * Returns the handle id. It is used to unregister the actor.
	 */
	UFUNCTION(BlueprintCallable, Category="BTW", meta=(DisplayName="RegisterActor"))
	FBTWActorRegisteredHandle RegisterActor(const TScriptInterface<IBTWCanJump>& Actor);

	/**
	 * Unbinds the handle associated with the ListenerId.
	 * The BreadcrumbsGenerator will stop listening to the jumps and falls from the actor associated with that handle.
	 */
	UFUNCTION(BlueprintCallable, Category="BTW", meta=(DisplayName="UnRegisterActor"))
	bool UnRegisterActor(const FBTWActorRegisteredHandle& ListenerId);

	/**
	 * Tries placing a link between the vector From and the vector To.
	 * If any of the vector cannot be projected to the nav mesh the link will not be placed.
	 * If the cost between these two points are less than the MinimumCostForNewLink the link will not be placed.
	 * Returns if the placing of the link was successful.
	 */
	bool PlaceBreadcrumb(const FNavAgentProperties& AgentProperties, const FVector& From, const FVector& To);

	void PathFoundCallback(uint32 QueryId, ENavigationQueryResult::Type Result, FNavPathSharedPtr Path);

private:
	/** Creates a NavLink between the vector From and the point To */
	void CreateNavLink(const FVector& From, const FVector& To) const;

	/** The extent that will be used when projecting the points to place a link */
	UPROPERTY(EditDefaultsOnly, Category="BTW", meta=(AllowPrivateAccess="true"))
	FVector NavMeshProjectionExtent = FVector(100.f, 100.f, 150.f);

	/** Minimum distance between to points for the link to be placed */
	UPROPERTY(EditDefaultsOnly, Category="BTW", meta=(AllowPrivateAccess="true"))
	float MinimumCostForNewLink = 200.f;

	/** Class that will be use to retrieve the Nav Links */
	UPROPERTY(EditDefaultsOnly, Category="BTW", meta=(AllowPrivateAccess="true"))
	TSubclassOf<UBTWNavLinkPooler> NavLinkPoolerClass;

	/** Used to retrieve the Nav Links */
	UPROPERTY()
	UBTWNavLinkPooler* NavLinkPooler;

	/** Current BreadcrumbActorListeners that are being listening to an specific actor */
	TMap<FBTWActorRegisteredHandle, class ABTWBreadcrumbActorListener*> ActorListeners;

	/** Queries currently executing to find the path between two points */
	TMap<uint32, FBreadcrumbToBePlaced> Queries;

	FNavPathQueryDelegate FindPathDelegate;
};
