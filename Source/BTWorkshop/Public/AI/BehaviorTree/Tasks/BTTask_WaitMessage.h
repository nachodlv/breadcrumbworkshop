﻿

#pragma once

#include "CoreMinimal.h"

#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_WaitMessage.generated.h"

struct FWaitMessageTaskMemory
{
	int32 MessagesReceived = 0;
	float RemainingWaitTime = 0.f;
	bool ShouldTimeout = false;
};

UCLASS()
class BTWORKSHOP_API UBTTask_WaitMessage : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_WaitMessage(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditDefaultsOnly, Category ="BTW", meta=(InlineEditConditionToggle))
	bool bQuantityOfMessageWithKey;

	UPROPERTY(EditAnywhere, Category="BTW", meta=(EditCondition = "bQuantityOfMessageWithKey"))
	FBlackboardKeySelector QuantityOfMessagesKey;

	UPROPERTY(EditAnywhere, Category="BTW", meta=(EditCondition = "!bQuantityOfMessageWithKey"))
	int32 QuantityOfMessages;

	UPROPERTY(EditAnywhere, Category="BTW")
	FName MessageToWait;

	// if timeout is <= 0 then the task doesn't timeout
	UPROPERTY(EditAnywhere, Category="BTW")
	float Timeout = 0.f;

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual uint16 GetInstanceMemorySize() const override;

protected:
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	virtual void OnMessage(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, FName Message, int32 RequestID, bool bSuccess) override;

private:
	int GetMaxQuantityOfMessage(UBehaviorTreeComponent& OwnerComp) const;
};
