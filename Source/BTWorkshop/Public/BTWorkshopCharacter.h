// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "AI/NavMesh/Interfaces/BTWCanJump.h"

#include "GameFramework/Character.h"
#include "BTWorkshopCharacter.generated.h"

UCLASS(config=Game)
class ABTWorkshopCharacter : public ACharacter, public IBTWCanJump
{
	GENERATED_BODY()
public:
	ABTWorkshopCharacter();

	void Shoot();

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	virtual void OnJumped_Implementation() override;
	virtual void OnWalkingOffLedge_Implementation(const FVector& PreviousFloorImpactNormal, const FVector& PreviousFloorContactNormal, const FVector& PreviousLocation, float TimeDelta) override;
	virtual void Landed(const FHitResult& Hit) override;

	// ~ Begin IBTWCanJump
	virtual FICanJumpDelegate& GetOnJumpDelegate() override { return OnJumpDelegate; }
	virtual FICanJumpDelegate& GetOnWalkOffLedgeDelegate() override { return OnWalkOffLedgeDelegate; }
	virtual FICanJumpDelegate& GetOnLandDelegate() override { return OnLandDelegate; }
	virtual const FNavAgentProperties& GetAgentProperties() override;
	// ~ End IBTWCanJump

private:
	UPROPERTY(EditAnywhere, Category = "BTW|Shooting", meta = (AllowPrivateAccess = "true"))
	float ShootingRange;

	UPROPERTY(EditAnywhere, Category = "BTW|Shooting", meta = (AllowPrivateAccess = "true"))
	UParticleSystem* ImpactEffect;

	FICanJumpDelegate OnJumpDelegate;
	FICanJumpDelegate OnWalkOffLedgeDelegate;
	FICanJumpDelegate OnLandDelegate;
};

