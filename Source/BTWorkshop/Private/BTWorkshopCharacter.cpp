#include "BTWorkshop/Public/BTWorkshopCharacter.h"


#include "DrawDebugHelpers.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"

ABTWorkshopCharacter::ABTWorkshopCharacter()
{

}

void ABTWorkshopCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ABTWorkshopCharacter::Shoot);
}

void ABTWorkshopCharacter::OnJumped_Implementation()
{
	Super::OnJumped_Implementation();
	OnJumpDelegate.Broadcast(GetActorLocation());
}

void ABTWorkshopCharacter::OnWalkingOffLedge_Implementation(const FVector& PreviousFloorImpactNormal,
	const FVector& PreviousFloorContactNormal, const FVector& PreviousLocation, float TimeDelta)
{
	Super::OnWalkingOffLedge_Implementation(PreviousFloorImpactNormal, PreviousFloorContactNormal, PreviousLocation, TimeDelta);
	OnWalkOffLedgeDelegate.Broadcast(PreviousLocation);
}

void ABTWorkshopCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);
	OnLandDelegate.Broadcast(Hit.Location);
}

const FNavAgentProperties& ABTWorkshopCharacter::GetAgentProperties()
{
	return GetNavAgentPropertiesRef();
}


void ABTWorkshopCharacter::Shoot()
{
	UWorld* World = GetWorld();
	FHitResult HitResult;
	const FVector& ActorLocation = GetActorLocation();
	const FVector& End = ActorLocation + GetActorForwardVector() * ShootingRange;
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	const bool Hit = World->LineTraceSingleByChannel(HitResult, ActorLocation, End, ECC_Pawn, Params);
	if (Hit && HitResult.Actor.Get())
	{
		DrawDebugLine(World, ActorLocation, HitResult.ImpactPoint, FColor::Red, false, 2.f);
		UGameplayStatics::SpawnEmitterAtLocation(this, ImpactEffect, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), true);
	}
}


