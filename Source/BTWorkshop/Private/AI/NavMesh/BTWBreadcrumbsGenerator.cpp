﻿#include "AI/NavMesh/BTWBreadcrumbsGenerator.h"

// UE Includes
#include "GameFramework/Character.h"
#include "NavigationSystem.h"
#include "NavLinkCustomComponent.h"

// BTW Includes
#include "AI/NavMesh/BTWBreadcrumbActorListener.h"
#include "AI/NavMesh/BTWNavLinkPooler.h"
#include "AI/NavMesh/Interfaces/BTWNavLinkProxyProvider.h"

ABTWBreadcrumbsGenerator::ABTWBreadcrumbsGenerator()
{
}

void ABTWBreadcrumbsGenerator::BeginPlay()
{
	Super::BeginPlay();
	NavLinkPooler = NewObject<UBTWNavLinkPooler>(this, NavLinkPoolerClass);
	FindPathDelegate.BindUObject(this, &ABTWBreadcrumbsGenerator::PathFoundCallback);
}

FBTWActorRegisteredHandle ABTWBreadcrumbsGenerator::RegisterActor(const TScriptInterface<IBTWCanJump>& Actor)
{
	ABTWBreadcrumbActorListener* Listener = GetWorld()->SpawnActor<ABTWBreadcrumbActorListener>();
	if (Listener)
	{
		Listener->Initialize(this);
		Listener->ListenToActor(Actor);
		FBTWActorRegisteredHandle Handle = FBTWActorRegisteredHandle::GenerateNewHandle();
		ActorListeners.Add(Handle, Listener);
		return Handle;
	}
	return FBTWActorRegisteredHandle();
}

bool ABTWBreadcrumbsGenerator::UnRegisterActor(const FBTWActorRegisteredHandle& ListenerId)
{
	ABTWBreadcrumbActorListener** Listener = ActorListeners.Find(ListenerId);
	if (Listener)
	{
		check(*Listener); // we should never have a null in the array
		ActorListeners.Remove(ListenerId);
		(*Listener)->BeginDestroy();
		return true;
	}

	return false;
}

void ABTWBreadcrumbsGenerator::BeginDestroy()
{
	for (auto Listener : ActorListeners)
	{
		if (IsValid(Listener.Value))
		{
			Listener.Value->BeginDestroy();
		}
	}
	Super::BeginDestroy();
}


bool ABTWBreadcrumbsGenerator::PlaceBreadcrumb(const FNavAgentProperties& AgentProperties, const FVector& From, const FVector& To)
{
	// TODO - Make the validity of the link and call ABTWBreadcrumbsGenerator::CreateNavLink
	// For example: Check the path distance between the two points (you can use the map Queries as a helper)
	// To get the point on the nav mesh use UNavigationSystemV1::ProjectPointToNavigation
	// To calculate the path between two points use UNavigationSystemV1::FindPathAsync

	UNavigationSystemV1* NavigationSystem = UNavigationSystemV1::GetCurrent(GetWorld());
	FNavLocation FromNav;
	FNavLocation ToNav;
	if (!NavigationSystem)
	{
		return false;
	}
	if (!NavigationSystem->ProjectPointToNavigation(From, FromNav, NavMeshProjectionExtent))
	{
		return false;
	}
	if (!NavigationSystem->ProjectPointToNavigation(To, ToNav, NavMeshProjectionExtent))
	{
		return false;
	}

	FPathFindingQuery Query;
	Query.CostLimit = MinimumCostForNewLink;
	Query.bAllowPartialPaths = false;
	Query.StartLocation = FromNav;
	Query.EndLocation = ToNav;

	uint32 QueryId = NavigationSystem->FindPathAsync(AgentProperties, Query, FindPathDelegate);
	Queries.Add(QueryId, FBreadcrumbToBePlaced(FromNav, ToNav));
	return true;
}

void ABTWBreadcrumbsGenerator::PathFoundCallback(uint32 QueryId, ENavigationQueryResult::Type Result,
	FNavPathSharedPtr Path)
{
	if (Result != ENavigationQueryResult::Success)
	{
		if (FBreadcrumbToBePlaced* BreadcrumbToBePlaced = Queries.Find(QueryId))
		{
			CreateNavLink(BreadcrumbToBePlaced->From, BreadcrumbToBePlaced->To);
		}
	}
	Queries.Remove(QueryId);
}

void ABTWBreadcrumbsGenerator::CreateNavLink(const FVector& From, const FVector& To) const
{
	if (!NavLinkPooler)
	{
		return;
	}
	const TScriptInterface<IBTWNavLinkProxyProvider>& Link = NavLinkPooler->GetNavLink();
	check(Link);
	UNavLinkCustomComponent* SmartLink = Link->GetSmartLink();
	SmartLink->SetLinkData(From, To, ENavLinkDirection::LeftToRight);
}

