﻿#include "AI/NavMesh/BTWBreadcrumbActorListener.h"

// UE Includes
#include "GameFramework/Character.h"

// BTW Includes
#include "AI/NavMesh/BTWBreadcrumbsGenerator.h"
#include "AI/NavMesh/Interfaces/BTWCanJump.h"

void ABTWBreadcrumbActorListener::Initialize(ABTWBreadcrumbsGenerator* NewBreadcrumbsGenerator)
{
	BreadcrumbsGenerator = NewBreadcrumbsGenerator;
}

void ABTWBreadcrumbActorListener::ListenToActor(const TScriptInterface<IBTWCanJump>& Actor)
{
	if (!Actor)
	{
		return;
	}

	ActorListened = Actor;
	JumpHandle = Actor->GetOnJumpDelegate().AddUObject(this, &ABTWBreadcrumbActorListener::ActorJumped);
	WalkOffLedgeHandle = Actor->GetOnWalkOffLedgeDelegate().AddUObject(this, &ABTWBreadcrumbActorListener::ActorWalkedOffLedge);
	LandHandle = Actor->GetOnLandDelegate().AddUObject(this, &ABTWBreadcrumbActorListener::ActorLanded);
}

void ABTWBreadcrumbActorListener::BeginDestroy()
{
	if (ActorListened)
	{
		ActorListened->GetOnJumpDelegate().Remove(JumpHandle);
		ActorListened->GetOnWalkOffLedgeDelegate().Remove(WalkOffLedgeHandle);
		ActorListened->GetOnLandDelegate().Remove(LandHandle);
	}
	Super::BeginDestroy();
}


void ABTWBreadcrumbActorListener::ActorJumped(const FVector& Location)
{
	From = Location;
}

void ABTWBreadcrumbActorListener::ActorLanded(const FVector& Location)
{
	if (BreadcrumbsGenerator)
	{
		BreadcrumbsGenerator->PlaceBreadcrumb(ActorListened->GetAgentProperties(), From, Location);
	}
}

void ABTWBreadcrumbActorListener::ActorWalkedOffLedge(const FVector& PreviousPosition)
{
	From = PreviousPosition;
}

