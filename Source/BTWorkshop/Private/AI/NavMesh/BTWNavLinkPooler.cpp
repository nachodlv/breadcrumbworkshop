﻿#include "AI/NavMesh/BTWNavLinkPooler.h"

// UE Includes
#include "Kismet/GameplayStatics.h"

// BTW Includes
#include "AI/NavMesh/Interfaces/BTWNavLinkProxyProvider.h"


// ~ Begin FNavLinkWrapper

FNavLinkWrapper::FNavLinkWrapper(const TScriptInterface<IBTWNavLinkProxyProvider> InNavLinkProvider) : NavLinkProvider(InNavLinkProvider)
{
	if (NavLinkProvider)
	{
		OnNavLinkReachedDelegate = NavLinkProvider->GetOnNavLinkProxyReached().AddRaw(this, &FNavLinkWrapper::NavLinkReached);
		LastTimeUsed = UGameplayStatics::GetRealTimeSeconds(NavLinkProvider.GetObject());
	}
}

FNavLinkWrapper::~FNavLinkWrapper()
{
	if (NavLinkProvider)
	{
		NavLinkProvider->GetOnNavLinkProxyReached().Remove(OnNavLinkReachedDelegate);
	}
}

void FNavLinkWrapper::NavLinkReached(AActor* Actor, const FVector& Destination)
{
	LastTimeUsed = UGameplayStatics::GetRealTimeSeconds(Actor);
	OnNavLinkUsed.Broadcast(*this);
}

// ~ End FNavLinkWrapper


// ~ Begin UBTWNavLinkPooler

UBTWNavLinkPooler::UBTWNavLinkPooler()
{
}

const TScriptInterface<IBTWNavLinkProxyProvider>& UBTWNavLinkPooler::GetNavLink()
{
	return InstantiateWrapper().GetNavLinkProvider();
}

void UBTWNavLinkPooler::BeginDestroy()
{
	NavLinkWrappers.Empty(); // Necessary for unbinding the NavLinkProvider on the destructor
	Super::BeginDestroy();
}

FNavLinkWrapper& UBTWNavLinkPooler::InstantiateWrapper()
{
	if (NavLinkWrappers.Num() >= MaxLinks)
	{
		int OldestLink = 0;
		for (int32 i = 1; i < NavLinkWrappers.Num(); ++i)
		{
			if (NavLinkWrappers[OldestLink].GetLastTimeUsed() > NavLinkWrappers[i].GetLastTimeUsed())
			{
				OldestLink = i;
			}
		}
		Cast<AActor>(NavLinkWrappers[OldestLink].GetNavLinkProvider().GetObject())->Destroy();
		NavLinkWrappers.RemoveAt(OldestLink, 1, false);
	}

	// Instantiates the new link
	AActor* LinkSpawned = GetWorld()->SpawnActor(LinkProxyClass);
	const TScriptInterface<IBTWNavLinkProxyProvider> Link (LinkSpawned);

	check(Link); // The Link should always be created

	// Adds the new link to the array of active links
	const int32 NewLink = NavLinkWrappers.Emplace(FNavLinkWrapper(Link));

	return NavLinkWrappers[NewLink];
}

// ~ End UBTWNavLinkPooler
