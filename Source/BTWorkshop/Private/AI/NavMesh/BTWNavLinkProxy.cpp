﻿#include "AI/NavMesh/BTWNavLinkProxy.h"

// UE Includes
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

ABTWNavLinkProxy::ABTWNavLinkProxy()
{
	SetSmartLinkEnabled(true);
	bSmartLinkIsRelevant = true;
}

void ABTWNavLinkProxy::BeginPlay()
{
	Super::BeginPlay();
	OnSmartLinkReached.AddDynamic(this, &ABTWNavLinkProxy::NotifySmartLinkReached);
}

void ABTWNavLinkProxy::NotifySmartLinkReached(AActor* PathingAgent, const FVector& DestPoint)
{
	OnNavLinkProxyReached.Broadcast(PathingAgent, DestPoint);

	ACharacter* Character = Cast<ACharacter>(PathingAgent);
	ensureMsgf(Character, TEXT("ABTWNavLinkProxy::NotifySmartLinkReached: the agent should implement ACharacter"));
	if (Character)
	{
		Character->LaunchCharacter(CalculateLaunchVelocity(Character, DestPoint), true, true);
	}
}

FVector ABTWNavLinkProxy::CalculateLaunchVelocity(const AActor* LaunchedActor, const FVector& Destination) const
{
	// Launch from the "feet" of the Character
	FVector Start = LaunchedActor->GetActorLocation();
	Start.Z -= LaunchedActor->GetSimpleCollisionHalfHeight();

	const FVector& End = GetActorTransform().TransformPosition(Destination);

	FVector Result;
	UGameplayStatics::SuggestProjectileVelocity_CustomArc(this, Result, Start, End);

	return Result;
}


