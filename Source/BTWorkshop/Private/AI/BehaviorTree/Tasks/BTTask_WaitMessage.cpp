﻿#include "AI/BehaviorTree/Tasks/BTTask_WaitMessage.h"

#include "BehaviorTree/BlackboardComponent.h"

UBTTask_WaitMessage::UBTTask_WaitMessage(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bNotifyTick = true;
	QuantityOfMessagesKey.AddIntFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_WaitMessage, QuantityOfMessagesKey));
}

EBTNodeResult::Type UBTTask_WaitMessage::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const float MaxQuantity = GetMaxQuantityOfMessage(OwnerComp);
	if (MaxQuantity <= 0)
	{
		return EBTNodeResult::Succeeded;
	}

	FWaitMessageTaskMemory* MessageTaskMemory = CastInstanceNodeMemory<FWaitMessageTaskMemory>(NodeMemory);
	MessageTaskMemory->MessagesReceived = 0;
	MessageTaskMemory->RemainingWaitTime = Timeout;
	MessageTaskMemory->ShouldTimeout = Timeout <= 0;

	WaitForMessage(OwnerComp, MessageToWait);

	return EBTNodeResult::InProgress;
}

uint16 UBTTask_WaitMessage::GetInstanceMemorySize() const
{
	return sizeof(FWaitMessageTaskMemory);
}

void UBTTask_WaitMessage::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	FWaitMessageTaskMemory* MessageTaskMemory = CastInstanceNodeMemory<FWaitMessageTaskMemory>(NodeMemory);
	if (MessageTaskMemory && MessageTaskMemory->ShouldTimeout)
	{
		MessageTaskMemory->RemainingWaitTime -= DeltaSeconds;
		if (MessageTaskMemory->RemainingWaitTime <= 0)
		{
			// FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
		}
	}
}

void UBTTask_WaitMessage::OnMessage(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, FName Message, int32 RequestID, bool bSuccess)
{
	if (MessageToWait != Message)
	{
		return;
	}

	FWaitMessageTaskMemory* MessageTaskMemory = CastInstanceNodeMemory<FWaitMessageTaskMemory>(NodeMemory);
	const float MaxQuantity = GetMaxQuantityOfMessage(OwnerComp);

	++MessageTaskMemory->MessagesReceived;
	UE_LOG(LogTemp, Warning, TEXT("Messages received %d"), MessageTaskMemory->MessagesReceived);
	const bool bMessagesReceived = MessageTaskMemory->MessagesReceived >= MaxQuantity;
	if (bMessagesReceived)
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}

int32 UBTTask_WaitMessage::GetMaxQuantityOfMessage(UBehaviorTreeComponent& OwnerComp) const
{
	UBlackboardComponent* BBComponent = OwnerComp.GetBlackboardComponent();
	int32 MaxQuantity = QuantityOfMessages;
	if (BBComponent && bQuantityOfMessageWithKey)
	{
		MaxQuantity = BBComponent->GetValueAsInt(QuantityOfMessagesKey.SelectedKeyName);
	}
	return MaxQuantity;
}
