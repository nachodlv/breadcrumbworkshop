// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BTWorkshopGameMode.generated.h"

UCLASS(minimalapi)
class ABTWorkshopGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABTWorkshopGameMode();
};



